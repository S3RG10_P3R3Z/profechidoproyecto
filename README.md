**Desarrollo Aplicaciones web con conexion a la base de datos**

**Alumno:**
*Sergio Guillermo Perez Perez*

**Semestre:**
*5AVP*

Practica #1 - 02/09/2022 - Práctica_ejemplo
Commit: 5ff803f77866d4e550192b912ca0ac70b6a4c290
Archivo: https://gitlab.com/S3RG10_P3R3Z/profechidoproyecto/-/blob/main/parcial1/Practica1.html

Practica #2 - 09/09/2022 - Práctica_JavaScript
Commit: ac77e6b6c173bdffd8b66e9f992ae2cf0a0aafdc
Archivo: https://gitlab.com/S3RG10_P3R3Z/profechidoproyecto/-/blob/main/parcial1/PracticaJavascript.html

Practica #3 - 17/09/2022 - Práctica WEB con base de datos
Commit: 3031e6f8f143f7b7b9aa3e689d1e2785d919772e
Archivo: https://gitlab.com/S3RG10_P3R3Z/profechidoproyecto/-/blob/main/parcial1/PracticaWebApp.rar

Practica #4 - 19/09/2022 - Práctica WEB Consulta de base de datos
Commit: 4df8c85e3879540ac02208be81acf198bb1b00f4
Archivo: https://gitlab.com/S3RG10_P3R3Z/profechidoproyecto/-/blob/main/parcial1/ConsultaDBaseDatos.html

Practica #5 - 23/09/2022 - Práctica WEB Registro de base de datos
Commit: bb652addd1da76ec1f701c4870b47f2234f54a8c
Archivo: https://gitlab.com/S3RG10_P3R3Z/profechidoproyecto/-/blob/main/parcial1/Practica_5.rar

Practica #6 - 26/09/2022 - Práctica WEB Conexion
Commit: 7063ad623057834dbb5d62df00fc2e279f05e148
Archivo: https://gitlab.com/S3RG10_P3R3Z/profechidoproyecto/-/blob/main/parcial1/Practica_6.rar


